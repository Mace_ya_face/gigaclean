﻿using System;
using System.IO;
using Ionic.Zip;
using SkiaSharp;
using PeanutButter.INIFile;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Text;

namespace GigaPixelClean
{
    class GigaClean
    {
        static void Main()
        {
            Console.WriteLine(" ");

            var settings = IniParse();
            if (settings == null) { return; }

            if (settings["Settings"]["CreateBackup"] == "true") { NewFolderBackup(settings["Directories"]["sourceDir"], settings["Directories"]["backupDir"]); }

            if (settings["Settings"]["DeleteWFSDuplicates"] == "true") { DeleteDuplicates(settings["Directories"]["sourceDir"]); }

            if (ImageSort(settings["Directories"]["sourceDir"], settings["Settings"]["TargetVerticalResolution"], settings["Settings"]["IrfanViewLocation"]) == 0)
            {
                WaitOnUser();
                return;
            }
            
            if (settings["Archive"]["EnableArchive"] == "true" && settings["Archive"]["EnableEncryption"] == "true")
            {
                CompressAndEncrypt(settings["Directories"]["sourceDir"], settings["Directories"]["archiveDir"], settings["Archive"]["ArchiveName"], settings["Archive"]["EncryptionPassword"]);
            }
            else if (settings["Archive"]["EnableArchive"] == "true") 
            {
                CompressAndEncrypt(settings["Directories"]["sourceDir"], settings["Directories"]["archiveDir"], settings["Archive"]["ArchiveName"]);
            }

            if (settings["Settings"]["CleanSourceFolderWhenCompleted"] == "true")
            {
                CopyAndReset(settings["Directories"]["sourceDir"], settings["Directories"]["destinationDir"], settings["Settings"]["SourceFolderStructure"]);
            }
            else if (settings["Settings"]["DeleteSourceFolderWhenCompleted"] == "true")
            {
                if (File.Exists(settings["Directories"]["sourceDir"])) { File.Delete(settings["Directories"]["sourceDir"]); }
            }

            if (settings["Settings"]["DeleteBackupWhenCompleted"] == "true")
            {
                if (File.Exists(settings["Directories"]["backupDir"])) { File.Delete(settings["Directories"]["backupDir"]); }
            }

            if (settings["Settings"]["CleanProcessingFoldersWhenCompleted"] == "true")
            {
                ClearProcessingFolders();
            }

            Console.WriteLine("Done!");
            WaitOnUser();
        }

        static INIFile IniParse()
        {
            if (!File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "GigaPixelClean.ini")))
            {
                Console.WriteLine("GigaPixelClean.ini is missing.");
                CreateBaseIni();
                Console.WriteLine("New ini file created. Pleae enter your settings there and re-run.");
                return null;
            }
            var settings = new INIFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "GigaPixelClean.ini"));
            return settings;
        }

        static void DeleteDuplicates(string folder)
        {
            DirectoryInfo pFileDir = new DirectoryInfo(folder);
            var paths = Directory.GetFiles(pFileDir.FullName, "*.*", SearchOption.AllDirectories);
            var fileCount = paths.Length;
            float fileCounter = -0.1f;

            foreach (var file in paths)
            {
                float per = fileCounter / fileCount * 100;
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                Console.WriteLine("Deleteing WFS Duplicates: " + Math.Round(per) + "%  ");

                var regex = new Regex(@".*\(\d+\)\..*");

                if (regex.IsMatch(file))
                {
                    File.Delete(file);
                }

                fileCounter += 1;
            }
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Console.WriteLine("Deleteing WFS Duplicates: 100%  ");
        }

        static void NewFolderBackup(string path, string bakPath)
        {
            Console.WriteLine("Running backup...");

            var gigaCleanBakPath = Path.Combine(Path.GetFullPath(bakPath) + "\\GigaCleanBak");

            if (Directory.Exists(gigaCleanBakPath)) { Directory.Delete(gigaCleanBakPath, true); }

            Directory.CreateDirectory(gigaCleanBakPath);

            DirectoryFileCopy(path, gigaCleanBakPath);
        }

        static int ImageSort(string folder, string vertReolutionTarget, string irfanLoc)
        {
            DirectoryInfo pFileDir = new DirectoryInfo(folder);
            var paths = Directory.GetFiles(pFileDir.FullName, "*.*", SearchOption.AllDirectories);
            var fileCount = paths.Length;
            float fileCounter = -0.1f;

            var twoFactorThreshold = Int32.Parse(vertReolutionTarget);
            var fourFactorThreshold = Int32.Parse(vertReolutionTarget) / 2;
            var sixFactorThreshold = Int32.Parse(vertReolutionTarget) / 4;

            Console.WriteLine(" ");

            foreach (var file in paths)
            {
                fileCounter += 1;
                float per = fileCounter / fileCount * 100;
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                Console.WriteLine("Sorting: " + Math.Round(per) + "%  ");

                if (file.ToUpper().EndsWith("JPEG") ||
                    file.ToUpper().EndsWith("JPG") ||
                    file.ToUpper().EndsWith("PNG") ||
                    file.ToUpper().EndsWith("TIFF") ||
                    file.ToUpper().EndsWith("RAW") ||
                    file.ToUpper().EndsWith("BMP") ||
                    file.ToUpper().EndsWith("JFIF") ||
                    file.ToUpper().EndsWith("EXIF") ||
                    file.ToUpper().EndsWith("HEIF"))
                {
                    if (File.Exists(file))
                    {
                        var folderImage = SKBitmap.Decode(file);

                        if(folderImage == null)
                        {
                            continue;
                        }

                        var filePath = file.Replace(folder, "");
                        filePath = filePath.Replace(Path.GetFileName(file), "");

                        if (folderImage.Height < sixFactorThreshold)
                        {
                            filePath = "6x" + filePath;
                            var fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + filePath);
                            Directory.CreateDirectory(fullPath);
                            var fullPathFile = fullPath + Path.GetFileName(file);
                            if (fullPathFile.Length > 255) { Console.WriteLine("\nERROR | File path too long on file: " + fullPathFile + "\nPlease restore your backup and try again"); WaitOnUser(); Environment.Exit(1); }
                            if (!File.Exists(fullPathFile)) { File.Move(file, fullPathFile); }
                            folderImage.Dispose();
                        }
                        else if (folderImage.Height < fourFactorThreshold)
                        {
                            filePath = "4x" + filePath;
                            var fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + filePath);
                            Directory.CreateDirectory(fullPath);
                            var fullPathFile = fullPath + Path.GetFileName(file);
                            if (fullPathFile.Length > 255) { Console.WriteLine("\nERROR | File path too long on file: " + fullPathFile + "\nPlease restore your backup and try again"); WaitOnUser(); Environment.Exit(1); }
                            if (!File.Exists(fullPathFile)) { File.Move(file, fullPathFile); }
                            folderImage.Dispose();
                        }
                        else if (folderImage.Height < twoFactorThreshold)
                        {
                            filePath = "2x" + filePath;
                            var fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + filePath);
                            Directory.CreateDirectory(fullPath);
                            var fullPathFile = fullPath + Path.GetFileName(file);
                            if (fullPathFile.Length > 255) { Console.WriteLine("\nERROR | File path too long on file: " + fullPathFile + "\nPlease restore your backup and try again"); WaitOnUser(); Environment.Exit(1); }
                            if (!File.Exists(fullPathFile)) { File.Move(file, fullPathFile); }
                            folderImage.Dispose();
                        }
                        else
                        {
                            folderImage.Dispose();
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    continue;
                }
            }

            Console.WriteLine("Images sorted!");

            var full2XDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "2x");
            var full4XDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "4x");
            var full6XDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "6x");

            if (!Directory.Exists(full2XDirectory) &&
                !Directory.Exists(full4XDirectory) &&
                !Directory.Exists(full6XDirectory))
            {
                Console.WriteLine("No images need upscaling!");
                return 0;
            }

            if (File.Exists(irfanLoc))
            {
                if (Directory.Exists(full2XDirectory))
                {
                    Console.WriteLine("Opening 2x files for upscale...");
                    Process.Start(irfanLoc, '"' + full2XDirectory + '"' + " /thumbs");
                    WaitOnUser();
                closeIrfan2x:
                    try
                    {
                        FolderCleanup(full2XDirectory);
                    }
                    catch
                    {
                        Console.WriteLine("Please close Irfanview");
                        WaitOnUser();
                        goto closeIrfan2x;
                    }
                    DirectoryFileCopy(full2XDirectory, folder);
                }

                if (Directory.Exists(full4XDirectory))
                {
                    Console.WriteLine("Opening 4x files for upscale...");
                    Process.Start(irfanLoc, '"' + full4XDirectory + '"' + " /thumbs");
                    WaitOnUser();
                closeIrfan4x:
                    try
                    {
                        FolderCleanup(full4XDirectory);
                    }
                    catch
                    {
                        Console.WriteLine("Please close Irfanview");
                        WaitOnUser();
                        goto closeIrfan4x;
                    }
                    DirectoryFileCopy(full4XDirectory, folder);
                }
                
                if (Directory.Exists(full6XDirectory))
                {
                    Console.WriteLine("Opening 6x files for upscale...");
                    Process.Start(irfanLoc, '"' + full6XDirectory + '"' + " /thumbs");
                    WaitOnUser();
                closeIrfan6x:
                    try
                    {
                        FolderCleanup(full6XDirectory);
                    }
                    catch
                    {
                        Console.WriteLine("Please close Irfanview");
                        WaitOnUser();
                        goto closeIrfan6x;
                    }
                    DirectoryFileCopy(full6XDirectory, folder);
                }
                
                return 1;
            }
            else
            {
                Console.WriteLine("Failed to find IrfanView executable. Please check your ini settings and restore your source folder from the backup before trying again.");
                return 0;
            }
        }

        static void CompressAndEncrypt(string folder, string archiveDir, string archiveName)
        {
            Console.WriteLine("Compressing...");

            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncoding = Encoding.GetEncoding(12000);
                zip.AlternateEncodingUsage = ZipOption.Always;
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                zip.AddDirectory(folder);
                zip.UseZip64WhenSaving = Zip64Option.AsNecessary;
                zip.Save(Path.Combine(archiveDir + "\\" + archiveName + ".zip"));
            }
        }

        static void CompressAndEncrypt(string folder, string archiveDir, string archiveName, string password)
        {
            Console.WriteLine("Compressing...");

            var tempArchive = archiveName + "_tmp.zip";

            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncoding = Encoding.GetEncoding(12000);
                zip.AlternateEncodingUsage = ZipOption.Always;
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                zip.AddDirectory(folder);
                zip.UseZip64WhenSaving = Zip64Option.AsNecessary;
                zip.Save(Path.Combine(archiveDir + "\\" + tempArchive));
            }

            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncoding = Encoding.GetEncoding(12000);
                zip.AlternateEncodingUsage = ZipOption.Always;
                zip.Encryption = EncryptionAlgorithm.WinZipAes256;
                zip.Password = password;
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.None;
                zip.AddItem(Path.Combine(archiveDir + "\\" + tempArchive), "");
                zip.UseZip64WhenSaving = Zip64Option.AsNecessary;
                zip.Save(Path.Combine(archiveDir + "\\" + archiveName + ".zip"));
            }

            if (File.Exists(Path.Combine(archiveDir + "\\" + tempArchive))) { File.Delete(Path.Combine(archiveDir + "\\" + tempArchive)); }
        }

        static void CopyAndReset(string sourceDir, string destinationDir, string folderStructure)
        {
            Directory.CreateDirectory(destinationDir);

            if (Directory.Exists(sourceDir)) { DirectoryFileCopy(sourceDir, destinationDir); }

            if (Directory.Exists(sourceDir)) { Directory.Delete(sourceDir, true); }

            var directories = folderStructure.Split(new char[] { '?', '?' });
            foreach (string path in directories) { Directory.CreateDirectory(sourceDir + path); }
        }

        static void CreateBaseIni()
        {
            File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "GigaPixelClean.ini"),

                "[Directories]" + Environment.NewLine +
                "sourceDir=" + Environment.NewLine +
                "destinationDir=" + Environment.NewLine +
                "; Backup directory must not match sourceDir" + Environment.NewLine +
                "backupDir=" + Environment.NewLine +
                "; Archive directory must not match sourceDir" + Environment.NewLine +
                "archiveDir=" + Environment.NewLine +
                "[Archive]" + Environment.NewLine +
                "; Will create archive of sourceDir after completetion of upscaling and cleaning," + Environment.NewLine +
                "; but before copying to destinationDir" + Environment.NewLine +
                "EnableArchive=false" + Environment.NewLine +
                "ArchiveName=" + Environment.NewLine +
                "EnableEncryption=false" + Environment.NewLine +
                "; Must be set if EnableEncryption=true" + Environment.NewLine +
                "EncryptionPassword=" + Environment.NewLine +
                "[Settings]" + Environment.NewLine +
                "TargetVerticalResolution=" + Environment.NewLine +
                "CreateBackup=true" + Environment.NewLine +
                "DeleteWFSDuplicates=false" + Environment.NewLine +
                "DeleteBackupWhenCompleted=false" + Environment.NewLine +
                "DeleteSourceFolderWhenCompleted=false" + Environment.NewLine +
                "CleanSourceFolderWhenCompleted=false" + Environment.NewLine +
                "CleanProcessingFoldersWhenCompleted=false" + Environment.NewLine +
                "; Separate multiple folders using \" ?? \". E.G --> \\Folder1\\Folder2??\\Folder3\\Folder4" + Environment.NewLine +
                "; This will create a structure of {sourceDir}\\Folder1\\Folder2" + Environment.NewLine +
                ";                                 {sourceDir}\\Folder3\\Folder4" + Environment.NewLine +
                "; Only needs to be set if CleanSourceFolderWhenCompleted=true" + Environment.NewLine +
                "SourceFolderStructure=" + Environment.NewLine +
                "IrfanViewLocation=C:\\Program Files\\IrfanView\\i_view64.exe" + Environment.NewLine +
                ";IrfanViewLocation=C:\\Program Files(x86)\\IrfanView\\i_view32.exe" + Environment.NewLine
            );

            Console.WriteLine("Please enter ini settings in the newly created file and restart GigaClean");
            WaitOnUser();
            Environment.Exit(1);
        }   

        static void FolderCleanup(string folder)
        {
            try
            {
                DirectoryInfo pFileDir = new DirectoryInfo(folder);
                var paths = Directory.GetFiles(pFileDir.FullName, "*.*", SearchOption.AllDirectories);
                var fileCount = paths.Length;
                float fileCounter = 0.1f;

                foreach (var file in paths)
                {
                    float per = fileCounter / fileCount * 100;
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    Console.WriteLine("Cleaning: " + Math.Round(per) + "%  ");

                    try
                    {
                        if (file.Contains("-gigapixel"))
                        {
                            string fileToDelete = file.Replace("-gigapixel", "");

                            try
                            {
                                if (File.Exists(fileToDelete))
                                {
                                    File.Delete(fileToDelete);
                                }
                                else if (File.Exists(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".jpeg"))
                                {
                                    File.Delete(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".jpeg");
                                }
                                else if (File.Exists(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".png"))
                                {
                                    File.Delete(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".png");
                                }
                                else if (File.Exists(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".tiff"))
                                {
                                    File.Delete(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".tiff");
                                }
                                else if (File.Exists(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".raw"))
                                {
                                    File.Delete(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".raw");
                                }
                                else if (File.Exists(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".bmp"))
                                {
                                    File.Delete(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".bmp");
                                }
                                else if (File.Exists(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".jfif"))
                                {
                                    File.Delete(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".jfif");
                                }
                                else if (File.Exists(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".exif"))
                                {
                                    File.Delete(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".exif");
                                }
                                else if (File.Exists(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".heif"))
                                {
                                    File.Delete(fileToDelete.Substring(0, fileToDelete.Length - 4) + ".heif");
                                }
                                else
                                {
                                    Console.WriteLine("Failed to delete: " + fileToDelete.Substring(0, fileToDelete.Length - 4) + " is missing");
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Failed to delete file: " + fileToDelete + "\nError: " + e);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to check file: " + file + "\nError: " + e);
                    }

                    fileCounter += 1;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to get directroy info.\nError: " + e);
            }
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Console.WriteLine("Cleaning: 100%  ");
        }

        static void ClearProcessingFolders()
        {
            if (Directory.Exists("./2x"))
            {
                Directory.Delete("./2x", true);
            }
            if (Directory.Exists("./4x"))
            {
                Directory.Delete("./4x", true);
            }
            if (Directory.Exists("./6x"))
            {
                Directory.Delete("./6x", true);
            }
        }

        static void DirectoryFileCopy(string sourceDir, string destDir)
        {
            if (!Directory.Exists(sourceDir)) { Console.WriteLine("Source directory not found."); return; }

            foreach (string dirPath in Directory.GetDirectories(sourceDir, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(sourceDir, destDir));
            }
            foreach (string newPath in Directory.GetFiles(sourceDir, "*.*", SearchOption.AllDirectories))
            {
                var newPathReplaced = newPath.Replace(sourceDir, destDir);
                if (newPathReplaced.Length > 255) { Console.WriteLine("\nERROR | File path too long on file: " + newPath + "\nPlease restore your backup and try again"); WaitOnUser(); Environment.Exit(1); }

                File.Copy(newPath, newPathReplaced, true);
            }
        }

        static void WaitOnUser()
        {
            Console.WriteLine("Press any key to continue...");

            bool userInput = false;
            while (userInput == false)
            {
                var waitOnInput = Console.ReadKey();

                if (waitOnInput.KeyChar == 0)
                {
                    continue;
                }
                else
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    Console.WriteLine("                            ");
                    return;
                }
            }
        }
    }
}